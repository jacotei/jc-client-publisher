<?php
class PublisherClient {

    private $apiUrl;
    private $apiKey;

    private $defaultHeaders = array(
            "Accept" => "application/json",
            "Content-Type" => "application/x-www-form-urlencoded"
    );

    public function __construct($apiKey="123456789",$host = "publisher.api.jacotei.com.br",$port="8080",$urlPath="jacotei-publisher",$protocol="http://") {
        $this->apiKey = $apiKey;
        $this->apiUrl = $protocol . $host. ":" . $port . "/" . $urlPath;
    }

    public function bloquear_publisher($offerId,$blockDate,$blockingTypeId,$blockingReasonId,$motive=null,$active=true,$blockingMethodId=0){

        $body =  array(
            "offerId" =>$offerId,
            "blockDate" => $blockDate,
            "active" => $active,
            "blockingTypeId" => $blockingTypeId,
            "blockingReasonId" => $blockingReasonId,
            "blockingMethodId" => $blockingMethodId,
            "motive" => $motive
        );

        $response = $this->requestPublisher("/offer/block",$body);
        return $response;
    }

    public function desbloquear_publisher($offerId){

        $body =  array(
            "offerId" =>$offerId
        );

        $response = $this->requestPublisher("/offer/unblock",$body);
        return $response;
    }

    public function associa_publisher($produto_id,$oferta_id){

        $body =  array(
            "offerIds" =>$oferta_id,
            "productId" => $produto_id
        );
        $response = $this->requestPublisher("/offer/assign",$body);
        return $response;
    }

    public function desativar_publisher($offerIds){

       $body = array(
            "offerIds" =>$offerIds
        );

        $response = $this->requestPublisher("/offer/unassign",$body);
        return $response;
    }

    public function publicarOFerta($offerId) {

        $body = array(
            "offerId" =>$offerId
        );

        $response = $this->requestPublisher("/offer/publish",$body);
        return $response;

    }

    public function atualizaLoja($storeId) {
        $body = array(
            "storeIds" =>$storeId
        );

        $response = $this->requestPublisher("/store/update",$body);
        return $response;
    }

    public function atualizaCampanha($campaignId) {
        $body = array(
            "campaignIds" =>$campaignId
        );

        $response = $this->requestPublisher("/store/campaign/update",$body);
        return $response;

    }

    private function requestPublisher($endPoint,$body = array(),$headers = array(),$requestType="post"){

        $url = $this->apiUrl . "$endPoint";
        $headers = array_merge($this->defaultHeaders,$headers);

        $body = http_build_query($body);

        $response = Unirest::$requestType($url,$headers,$body);
        return $response;

    }

}
